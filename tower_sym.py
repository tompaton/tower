import math

import draw_tower

class Component(object):
    def __init__(self):
        self.children = []  # array of parallel arrays of sequential components
        self.due = None
        self.start_t = None
        self.cost = 1.0  # leaf Components will cost at least 1.0 time unit

    def add_children(self, children):
        """add sequential list of child components"""
        self.children.append(children)

    def required_time(self, due=0.0):
        self.due = due
        self.start_t = due - self.cost

        for children in self.children:
            # each set of children is due to finish at the same time
            due = self.due

            for child in children:
                due -= child.required_time(due)

            # must start early enough to finish longest sequence of children
            if self.start_t > due:
                self.start_t = due

        return self.due - self.start_t

    def draw(self, t, **kwargs):
        if self.children:
            kwargs = kwargs.copy()
            kwargs.update(self.draw_kwargs(t))
            for children in self.children:
                for child in children:
                    child.draw(t, **kwargs)
        else:
            raise NotImplementedError(
                'draw must be overridden for leaf Components')

    def draw_kwargs(self, t):
        return {}

    def color(self, t):
        if t >= self.due:
            return (0.75, 0.75, 0.75)
        else:
            return (0.25, 0.25, 0.25)

    def draw_schedule(self, t, draw):
        if self.children:
            for children in self.children:
                for child in children:
                    child.draw_schedule(t, draw)
        else:
            draw(self.start_t, self.due, self.color(t))

    def schedule_depth(self):
        return self._walk_children('schedule_depth', (), sum, 0.25)

    def _walk_children(self, attrib, args, agg, default):
        if self.children:
            return agg(getattr(child, attrib)(*args)
                       if hasattr(child, attrib)
                       else child._walk_children(attrib, args, agg, default)
                       for children in self.children
                       for child in children)
        else:
            return default

class Tower(Component):
    def __init__(self, layers=20, height=5.0):
        super(Tower, self).__init__()

        self.height = height
        layer_height = height / layers
        self.add_children([Layer(base=i*layer_height - height,
                                 height=layer_height,
                                 width=1.0 - 0.75*(i/float(layers)))
                           for i in range(layers)])

    def draw_kwargs(self, t):
        return {'tower_offset': self.tower_offset(t)}

    def tower_offset(self, t):
        return self._walk_children('tower_offset', (t,), sum, 0.0)

class Layer(Component):
    def __init__(self, base=0.0, height=1.0, width=1.0):
        super(Layer, self).__init__()

        self.base = base
        self.height = height
        self.width = width

        primaries = Primaries(self.height)
        legs = []
        for leg in range(0, Primary.count):
            legs.append(Primary(leg,
                                radius=self.width,
                                base=self.base,
                                height=self.height,
                                width=0.05 * self.width))
            primaries.add_children([legs[-1]])

        braces = Braces()
        for leg in range(0, Primary.count):
            leg1 = legs[leg]
            leg2 = legs[(leg + 1) % Primary.count]
            leg3 = legs[(leg + 3) % Primary.count]

            braces.add_children([Brace(leg1, leg2)])
            braces.add_children([Brace(leg1, leg3)])

        self.add_children([braces, primaries])

class Primaries(Component):
    def __init__(self, height=1.0):
        super(Primaries, self).__init__()

        self.height = height

    def tower_offset(self, t):
        # count completed components
        if t >= self.due:
            return self.height
        else:
            return 0.0

class Primary(Component):
    count = 6

    def __init__(self, leg=0, radius=1.0, base=0.0, height=1.0, width=1.0):
        super(Primary, self).__init__()

        self.height = height
        self.width = width
        self.x = radius * math.cos(2 * math.pi * leg / self.count)
        self.y = base + 0.5 * self.height
        self.z = radius * math.sin(2 * math.pi * leg / self.count)

    def draw(self, t, tower_offset=0.0, **kwargs):
        draw_tower.cube(offset=(self.x, tower_offset + self.y, self.z),
                        scale=(self.width, 0.5 * self.height, self.width),
                        color=self.color(t))

class Braces(Component):
    pass

class Brace(Component):
    def __init__(self, primary1, primary2):
        super(Brace, self).__init__()

        self.primary1 = primary1
        self.primary2 = primary2

    def draw(self, t, tower_offset=0.0, **kwargs):
        p1 = self.primary1
        p2 = self.primary2
        draw_tower.brace((p1.x, tower_offset + p1.y - 0.25 * p1.height, p1.z),
                         (p2.x, tower_offset + p2.y + 0.25 * p2.height, p2.z),
                         color=self.color(t))
