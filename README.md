# Tall Tower Simulation #

Simulation of the construction of a Tall Tower, a la http://hieroglyph.asu.edu/project/the-tall-tower/

Written in Python using pyglet and OpenGL.

* Recommended: setup a virtualenv
* pip install -r requirements.txt
* ./tower.py

Very basic at this stage, the intention is not to focus on the structural simulation (i.e we'll assume the loads and stresses have been figured out already), but to work through the logistics of the construction to determine the amount of time required and the project feasibility.

The simulation should pre-compute information so that it can be drawn at any time and run forwards or backwards in time.

## Basic window controls ##

* drag with the mouse to tilt and rotate the tower
* scroll-wheel to zoom in and out

* click on the timeline to change the time
* scroll-wheel to see more of the timeline

* keys
    - [space] pause
    - [up] jump to the end
    - [down] jump to the start
    - [left] back one time step
    - [right] forward one time step

## Screen shot ##

![Screen shot](https://bitbucket.org/tompaton/tower/raw/9e7932b0f5b1685022c22e1f41aaa05906006280/screenshots/screenshot-1.png "Tower simulation and timeline windows")
