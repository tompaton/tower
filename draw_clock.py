from pyglet import gl

def schedule_projection():
    gl.glClear(gl.GL_COLOR_BUFFER_BIT)
    gl.glLoadIdentity()

def clock_projection(width, height, start_t, end_t):
    gl.glTranslatef(width - 10, height - 20, 0.0)
    gl.glScalef((width - 80) / (end_t - start_t), -10, 1.0)

def timeline_projection(width, height, scroll_y):
    gl.glEnable(gl.GL_SCISSOR_TEST)
    gl.glScissor(70, 10, width - 80, height - 40)
    gl.glTranslatef(0, scroll_y, 0)

def end_timeline_projection():
    gl.glDisable(gl.GL_SCISSOR_TEST)

def draw_clock(start_t, end_t, t):
    gl.glBegin(gl.GL_LINES)

    gl.glColor3f(0.25, 0.25, 0.25)
    gl.glVertex2f(start_t, 0.0)
    gl.glVertex2f(start_t, 1.0)
    gl.glVertex2f(end_t, 0.0)
    gl.glVertex2f(end_t, 1.0)

    gl.glColor3f(0.5, 0.5, 0.5)
    gl.glVertex2f(start_t, 0.5)
    gl.glVertex2f(t, 0.5)

    gl.glColor3f(0.15, 0.15, 0.15)
    gl.glVertex2f(t, 0.5)
    gl.glVertex2f(t, 500)

    gl.glEnd()

def draw_duration(start_t, due, color):
    gl.glTranslatef(0, 0.25, 0)

    gl.glBegin(gl.GL_LINES)
    gl.glColor3f(*color)
    gl.glVertex2f(start_t, 0.5)
    gl.glVertex2f(due, 0.5)
    gl.glEnd()
