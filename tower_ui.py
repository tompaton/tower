import math
import pyglet
from pyglet.window import key, mouse

import draw_clock
import draw_tower

class Camera(object):
    def __init__(self):
        self.view_angle = 0.0
        self.zoom = -10
        self.pitch = 0.0

        # rotate once per minute
        self.speed = 360.0 / 60.0

    def set_speed(self, dx, width):
        self.speed += 90.0 * dx / float(width)

    def set_pitch(self, dy, height):
        self.pitch -= 90.0 * dy / float(height)

    def set_zoom(self, scroll_y):
        self.zoom *= 1.0 + (scroll_y / 10.0)

    def rotate(self, dt):
        self.view_angle += self.speed * dt

class Clock(object):
    def __init__(self, epoch=0):
        self.start_t = -epoch
        self.end_t = 0
        self.paused = False
        self.t = self.start_t
        self.waited = 0.0
        self.speed_up = 2

    def tick(self, dt):
        if not self.paused:
            self.waited += dt * self.speed_up
            if self.waited > 1.0:
                self.waited = 0.0
                if self.t < self.end_t:
                    self.t += 1.0

    def set_t(self, x, width):
        t = (self.start_t
             + ((x - 70) / float(width - 80))
             * (self.end_t - self.start_t))

        if t < self.start_t:
            self.t = self.start_t
        elif t > self.end_t:
            self.t = self.end_t
        else:
            self.t = math.floor(t)

class ViewWindow(object):
    def __init__(self):
        self.window = pyglet.window.Window(1024, 768,
                                           resizable=True,
                                           caption="Tower simulation")
        self.fps_display = pyglet.window.FPSDisplay(self.window)

    def draw(self, camera, draw_world):
        self.window.switch_to()
        draw_tower.view_projection(self.window.width,
                                   self.window.height,
                                   camera.view_angle,
                                   camera.pitch,
                                   camera.zoom)
        self.fps_display.draw()
        draw_world()
        self.window.flip()

class ClockWindow(object):
    def __init__(self):
        self.window = pyglet.window.Window(512, 768,
                                           resizable=True,
                                           caption="Tower timeline")

        self.schedule_y = 0.5

        self.label_t = pyglet.text.Label('t', x=10, y=10, anchor_y='top',
                                         font_size=8)
        self.label_paused = pyglet.text.Label('paused', x=10, y=10,
                                              anchor_y='top',
                                              color=(128, 128, 128, 255),
                                              font_size=8)

    def scroll(self, height, scroll_y, depth):
        if scroll_y > 0:
            dy = scroll_y ** 2
        else:
            dy = -1 * scroll_y ** 2

        window_y = (height - 40) / 10.0
        self.schedule_y = max(0.5 + window_y - depth,
                              min(0.5, self.schedule_y + dy))

    def draw(self, clock, draw_world):
        self.window.switch_to()
        draw_clock.schedule_projection()

        self.label_t.y = self.window.height - 5
        self.label_t.text = 'T%.1f' % clock.t
        self.label_t.draw()

        if clock.paused:
            self.label_paused.y = self.window.height - 15
            self.label_paused.draw()

        draw_clock.clock_projection(self.window.width,
                                    self.window.height,
                                    clock.start_t,
                                    clock.end_t)

        draw_clock.draw_clock(clock.start_t, clock.end_t, clock.t)


        draw_clock.timeline_projection(self.window.width,
                                       self.window.height,
                                       self.schedule_y)

        draw_world()

        draw_clock.end_timeline_projection()

        self.window.flip()

class UI(object):
    def __init__(self, world):
        self.world = world
        self.camera = Camera()
        self.clock = Clock(world.required_time)

        self.window_view = ViewWindow()
        self.window_schedule = ClockWindow()

    def run(self):
        self.window_view.window.push_handlers(
            on_draw=self.on_draw_view,
            on_key_press=self.on_key_press,
            on_mouse_drag=self.on_drag_view,
            on_mouse_scroll=self.on_scroll_view,
            on_close=pyglet.app.exit)
        self.window_schedule.window.push_handlers(
            on_draw=self.on_draw_schedule,
            on_key_press=self.on_key_press,
            on_mouse_release=self.on_click_schedule,
            on_mouse_scroll=self.on_scroll_schedule,
            on_close=pyglet.app.exit)

        pyglet.clock.schedule_interval(self.camera.rotate, 0.1)
        pyglet.clock.schedule(self.clock.tick)
        pyglet.app.run()

    def on_draw_view(self):
        def draw_world():
            self.world.draw(self.clock.t)

        self.window_view.draw(self.camera, draw_world)

    def on_draw_schedule(self):
        def draw_world():
            self.world.draw_schedule(self.clock.t, draw_clock.draw_duration)

        self.window_schedule.draw(self.clock, draw_world)

    def on_key_press(self, symbol, modifiers):
        if symbol == key.SPACE:
            self.clock.paused = not self.clock.paused
        elif symbol == key.UP:
            self.clock.t = self.clock.end_t
        elif symbol == key.LEFT:
            self.clock.paused = True
            self.clock.t = max(self.clock.start_t, self.clock.t - 1.0)
        elif symbol == key.RIGHT:
            self.clock.paused = True
            self.clock.t = min(self.clock.end_t, self.clock.t + 1.0)
        elif symbol == key.DOWN:
            self.clock.t = self.clock.start_t

    def on_click_schedule(self, x, y, button, modifiers):
        if button == mouse.LEFT:
            self.clock.set_t(x, self.window_schedule.window.width)

    def on_drag_view(self, x, y, dx, dy, buttons, modifiers):
        if buttons & mouse.LEFT:
            self.camera.set_speed(dx, self.window_view.window.width)
            self.camera.set_pitch(dy, self.window_view.window.height)

    def on_scroll_view(self, x, y, scroll_x, scroll_y):
        self.camera.set_zoom(scroll_y)

    def on_scroll_schedule(self, x, y, scroll_x, scroll_y):
        self.window_schedule.scroll(self.window_schedule.window.height,
                                    scroll_y,
                                    self.world.schedule_depth)
