#!/usr/bin/env python
import draw_tower
import tower_sym
import tower_ui

class World(object):
    def __init__(self):
        self.tower = tower_sym.Tower()
        self.required_time = self.tower.required_time(0.0)
        self.schedule_depth = self.tower.schedule_depth()

    def draw(self, t):
        draw_tower.plane()
        self.tower.draw(t)

    def draw_schedule(self, t, draw):
        self.tower.draw_schedule(t, draw)

if __name__ == "__main__":
    tower_ui.UI(World()).run()
