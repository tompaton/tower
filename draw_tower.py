import pyglet
from pyglet import gl

def view_projection(width, height, view_angle, pitch, zoom):
    gl.glEnable(gl.GL_CULL_FACE)
    gl.glEnable(gl.GL_DEPTH_TEST)
    gl.glCullFace(gl.GL_BACK)

    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glLoadIdentity()
    gl.gluPerspective(45, width / float(height), .1, 1000)
    gl.glTranslatef(0.0, -2.0, zoom)
    gl.glRotatef(pitch, 1, 0, 0)
    gl.glRotatef(view_angle, 0, 1, 0)

    gl.glMatrixMode(gl.GL_MODELVIEW)
    gl.glLoadIdentity()
    gl.glClear(gl.GL_COLOR_BUFFER_BIT|gl.GL_DEPTH_BUFFER_BIT)

_CUBE_VERTICES = [1, -1, -1,
                  1, 1, -1,
                  -1, 1, -1,
                  -1, -1, -1,
                  1, -1, 1,
                  1, 1, 1,
                  -1, -1, 1,
                  -1, 1, 1]

_CUBE_EDGES = (0, 1, 0, 3, 0, 4,
               2, 1, 2, 3, 2, 7,
               6, 3, 6, 4, 6, 7,
               5, 1, 5, 4, 5, 7)

_CUBE_FACES = (3, 2, 1, 0,
               4, 5, 7, 6,
               0, 1, 5, 4,
               2, 3, 6, 7,
               1, 2, 7, 5,
               4, 6, 3, 0)

def cube(offset=(0.0, 0.0, 0.0), scale=(1.0, 1.0, 1.0), color=(1.0, 1.0, 1.0)):
    gl.glPushMatrix()
    gl.glTranslatef(*offset)
    gl.glScalef(*scale)
    gl.glColor3f(0.1, 0.1, 0.1)
    gl.glPolygonMode(gl.GL_FRONT, gl.GL_FILL)
    pyglet.graphics.draw_indexed(len(_CUBE_VERTICES) / 3,
                                 gl.GL_QUADS,
                                 _CUBE_FACES,
                                 ('v3i', _CUBE_VERTICES))
    gl.glColor3f(*color)
    gl.glPolygonMode(gl.GL_FRONT, gl.GL_LINE)
    gl.glLineWidth(2.0)
    pyglet.graphics.draw_indexed(len(_CUBE_VERTICES) / 3,
                                 gl.GL_QUADS,
                                 _CUBE_FACES,
                                 ('v3i', _CUBE_VERTICES))
    gl.glPopMatrix()
    gl.glPolygonMode(gl.GL_FRONT, gl.GL_FILL)

def brace(corner1, corner2, color):
    gl.glLineWidth(1.0)
    gl.glBegin(gl.GL_LINES)
    gl.glColor3f(*color)

    gl.glVertex3f(*corner1)
    gl.glVertex3f(*corner2)

    # swap y coordinates for other diagonal
    gl.glVertex3f(corner1[0], corner2[1], corner1[2])
    gl.glVertex3f(corner2[0], corner1[1], corner2[2])

    gl.glEnd()

def plane(offset=0.0, step=0.25, tiles=8):
    gl.glLineWidth(1.0)
    gl.glBegin(gl.GL_LINES)
    max_i = int(tiles / step)
    for i in range(0, max_i + 1):
        if i % int(1.0 / step) == 0:
            gl.glColor3f(0.5, 0.5, 0.5)
        else:
            gl.glColor3f(0.25, 0.25, 0.25)
        j = step * (i - max_i / 2)
        k = step * (max_i / 2)
        gl.glVertex3f(j, offset, k)
        gl.glVertex3f(j, offset, -k)
        gl.glVertex3f(k, offset, j)
        gl.glVertex3f(-k, offset, j)
    gl.glEnd()
